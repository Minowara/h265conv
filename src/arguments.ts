import colors from "colors";
import * as Path from "path";

const optional = require("optional");
const userSettings = optional("./settings.json") || {};

class Arguments {

    yargs: any;

    constructor(yargs: any) {
        this.yargs = yargs;
    }

    public getCLIArguments() {
        return this.yargs
            .usage(colors.underline('Usage:') + ' $0 [options] file|directory')
            .options({
                'b': {
                    alias: 'video-bitrate',
                    default: userSettings['video-bitrate'] || 0,
                    describe: 'Sets the video bitrate, set to 0 to use qp rate control instead of a target bitrate.',
                    type: 'number',
                    group: 'Encoder:'
                },
                'bs': {
                    alias: 'video-bitrate-same',
                    default: userSettings['video-bitrate-same'] || false,
                    describe: 'Sets the video bitrate, set to 0 to use qp rate control instead of a target bitrate.',
                    type: 'boolean',
                    group: 'Encoder:'
                },
                'c': {
                    alias: 'container',
                    default: userSettings['container'] || 'mkv',
                    describe: 'Output container format.',
                    choices: ['mkv', 'mp4', 'm4v'],
                    type: 'string',
                    group: 'Encoder:'
                },
                'debug': {
                    default: userSettings['debug'] || false,
                    describe: 'Enables debug mode. Prints extra debugging information.',
                    type: 'boolean',
                    group: 'Advanced:'
                },
                'd': {
                    alias: 'delete',
                    default: userSettings['delete'] || false,
                    describe: 'Delete source after encoding is completed. [DANGER]',
                    type: 'boolean',
                    group: 'Advanced:'
                },
                'l': {
                    alias: 'language',
                    default: userSettings['language'] || 'eng',
                    describe: 'The native language used to select default audio and subtitles. You may use 3 letter or 2 letter ISO 639-2 Alpha-3/Alpha-2 codes or the full language name. Examples: [eng|en|English|jpn|ja|Japanese]',
                    type: 'string',
                    group: 'Encoder:'
                },
                'o': {
                    alias: 'output',
                    default: userSettings['output'] || Path.resolve(process.cwd(), 'out'),
                    describe: 'Folder where encoded files are output.',
                    type: 'string',
                    normalize: true,
                    group: 'General:'
                },
                'p': {
                    alias: 'preset',
                    default: userSettings['preset'] || 'fast',
                    describe: 'x265 encoder preset.',
                    choices: ['ultrafast', 'superfast', 'veryfast', 'faster', 'fast', 'medium', 'slow', 'slower', 'veryslow', 'placebo'],
                    type: 'string',
                    group: 'Encoder:'
                },
                'q': {
                    alias: 'quality',
                    default: userSettings['quality'] || 19,
                    describe: 'Sets the qp quality target',
                    type: 'number',
                    group: 'Encoder:'
                },
                'r': {
                    alias: 'replace',
                    default: userSettings['replace'] || false,
                    describe: 'Enable override mode. Allows conversion of videos that are already encoded by the hevc codec.',
                    type: 'boolean',
                    group: 'General:'
                },
                'stats': {
                    default: userSettings['stats'] || false,
                    describe: 'Output a stats file containing stats for each video converted.',
                    type: 'boolean',
                    group: 'Advanced:'
                },
                't': {
                    alias: 'two-pass',
                    default: userSettings['mutli-pass'] || false,
                    describe: 'Enable two-pass by the encoder.',
                    type: 'boolean',
                    group: 'Encoder:'
                },
                'v': {
                    alias: 'verbose',
                    default: userSettings['verbose'] || false,
                    describe: 'Enables verbose mode. Prints extra information.',
                    type: 'boolean',
                    group: 'General:'
                },
                'x': {
                    alias: 'extra-options',
                    default: userSettings['extra-options'] || '',
                    describe: 'Extra x265 options. Options can be found on the x265 options page.',
                    type: 'string',
                    group: 'Encoder:'
                },
                'h': {
                    alias: "help",
                    describe: 'Displays help page.',
                    group: 'Options:'
                },
                'version': {
                    describe: 'Displays version information.',
                    group: 'Options:'
                }
            })
            .argv;
    }

    public showHelp() {
        this.yargs.showHelp();
    }
}

module.exports = function (yargs: any) {
    return new Arguments(yargs);
};