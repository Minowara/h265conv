# h265conv

h265conv is a fire and forget weapon. A nodejs utility utilizing ffmpeg to encode large quantities of videos with the hevc codec.

[![NPM License](https://img.shields.io/npm/l/h265conv.svg)](https://raw.githubusercontent.com/Minowara/h265conv/master/LICENSE) [![NPM Version](https://img.shields.io/npm/v/h265conv.svg)](https://www.npmjs.com/package/h265conv)

[![NPM Version](https://nodei.co/npm/h265conv.png)](https://www.npmjs.com/package/h265conv)

If you have any questions or h265conv isn't working for you, feel free to open an issue.

## Features

- Works on Windows, macOS, and Linux
- Batch file processing
- Automatically detects video files
- Detects all audio tracks
- Preserves audio codecs
- Preserves audio track titles
- Detects and preserves all subtitles
- Verbose mode
- File overwrite detection (doesn't accidentally write over a file that already exists, other than in preview mode)
- Detects if file is already encoded in x265 and skips it

### Future
- Support for 10bit & 12bit video
- Audio encoding

## Dependencies

- [Node.js](https://nodejs.org/en/) - Required in order to run h265conv.
- [ffmpeg](https://ffmpeg.org/) - Does the video conversion among other things.


## Installation

To install h265conv run one of the following command lines to download and install.

### Stable

```
npm install h265conv --global
```

### Bleeding Edge

```
npm install minowara/h265ize --global
h265conv --version
```

## Updating

Simply run `npm install h265conv --global` again.

## Uninstalling

`npm uninstall h265conv --global`

## Usage

`h265conv [options] file|directory`

### Options

> TBD

Run `h265conv --help` for more info.

### Examples

- `h265ize -v video_1080p_h264.mov`
- `h265ize -v -d /home -q 25 video_folder`

## Stats file

The stats file is located at the current working directory under the name `h265conv.csv`. This must be enabled using the `--stats` flag. The file is composed of several lines. Each line is in the format

`[Finish Encoding Date],[File Path],[Original Size],[Encoded size],[Compression Precent],[Encoding Duration]`

For example:

`08/13 02:46:03 PM, videos/[deanzel] Noir - 08 [BD 1080p Hi10p Dual Audio FLAC][a436a4e8].mkv, 1964MB, 504MB, 25.66%, 2:51:16`
